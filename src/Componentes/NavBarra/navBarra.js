import React, { Component } from 'react';
import logo from '../../Resources/Images/logo.svg';

export default class navBarra extends Component {
  render() {
    return (
        <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="/#">
            <img src={logo} width="30" height="30" className="d-inline-block align-top" alt=""/> APP DIEGO
            </a>
        </nav>

    );
  }
}
