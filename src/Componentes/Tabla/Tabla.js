import React, { Component } from 'react';

class Tabla extends Component {
  render() {
    return (
        <div className="col-md-6 offset-md-3 p-5">
            <table className="table">
            <thead className="thead-dark">
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Tarea</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    
                </tr>
            </tbody>

            </table>
        </div>
        
    );
  }
}

export default Tabla;
