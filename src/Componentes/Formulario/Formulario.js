import React, { Component } from 'react';

export default class Formulario extends Component {

  constructor(props){
    super(props);
    this.state = {
      tarea : ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      tarea : e.target.value
    });
  }

  handleSubmit(e) {
    alert('TAREA ' + this.state.tarea);
    e.preventDefault();
  }


  render() {
    return (
      <div className="col-md-6 offset-md-3 p-5">
          <form>
            <div class="form-row ">
                <div className="col-sm-10 my-1">
                <label className="sr-only" for="inlineFormInputName">Name</label>
                <input type="text" className="form-control" value={this.state.tarea} id="inlineFormInputName" onChange={this.handleChange} placeholder="Jane Doe"/>
                </div>
                
                <div class="col-sm-2 my-1">
                <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}  >Añadir Tarea</button>
                </div>
            </div>
        </form>
      </div>
        
    );
  }
}
