import React from 'react';
import './App.css';
import NavBarra from './Componentes/NavBarra/navBarra';
import Tabla from './Componentes/Tabla/Tabla';
import Formulario from './Componentes/Formulario/Formulario';
function App() {
  return (
    
    <div>
      <NavBarra/>
      <Tabla/>
      <Formulario/>
    </div>
    
    
    
  );
}

export default App;
